﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorCheck : MonoBehaviour
{
    public GameObject doorLocked;
    public GameObject doorUnlocked;
    public LevelProperties level;
    public bool completeLevel;
    public PlayerController player;
    public Transform wizard;
    public Animator anim;
    public GameObject particles;

    [Header("Sound Properties")]
    public AudioSource soundFX;
    public AudioClip doorUnlock;

    void Start()
    {
        doorLocked.SetActive(true);
        doorUnlocked.SetActive(false);
    }

    void Update()
    {
        if(completeLevel)
        {
            level.delayTime -= Time.deltaTime;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log("Check key");
            if(level.hasKey)
            {
                //next level stuff
                completeLevel = true;
                soundFX.PlayOneShot(doorUnlock);
                player.speed = 0f;
                anim.SetBool("WonLevel", true);
                Instantiate(particles, wizard);
            }
            else
            {
                //NO KEY STUFF
                Debug.Log("Doesn't have key");
            }
        }
    }
}
