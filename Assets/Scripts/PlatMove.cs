﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatMove : MonoBehaviour
{
    public UnityEngine.Vector3[] points;
    public int point_number = 0;
    private UnityEngine.Vector3 currentTarget;

    public float tolerence;
    public float speed;
    public float delayTime;

    private float delayedStart;

    public bool automatic;

    // Start is called before the first frame update
    void Start()
    {
        if (points.Length > 0)
        {
            currentTarget = points[0];
        }
        tolerence = speed * Time.deltaTime;


    }

    private void FixedUpdate()
    {
        if (transform.position != currentTarget)
        {
            MovePlatform();
        }
        else
        {
            Updatetarget();
        }
    }

    void MovePlatform()
    {
        UnityEngine.Vector3 heading = currentTarget - transform.position;
        transform.position += (heading / heading.magnitude) * speed * Time.deltaTime;
        if (heading.magnitude < tolerence)
        {
            transform.position = currentTarget;
            delayedStart = Time.time;
        }
    }
    void Updatetarget()
    {
        if (automatic)
        {
            if (Time.time - delayedStart > delayTime)
            {
                nextplatform();
            }
        }
    }

    public void nextplatform()
    {
        point_number++;
        if (point_number >= points.Length)
        {
            point_number = 0;
        }
        currentTarget = points[point_number];
    }

    private void OnTriggerEnter(Collider other)
    {
        other.transform.parent = transform;
    }

    private void OnTriggerExit(Collider other)
    {
        other.transform.parent = null;
    }
}
