﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spikesTrigger : MonoBehaviour
{
    /// DO NOT TOUCH THE CODE, I SWEAR TO GOD THE MOVEMENT OF THE SPIKES IS GOING TO BREAK, ASK ME (MOHAMED ELFAYOUMY) OR AMY BEFORE MESSING WITH THIS ///
    /// THE MOVEING SPIKES PREFAB IS HOLDING TIGHTLY AND WILL CORRUPT IF ANY ONE CHANGED THE POSITION OF ANY THING ///
    /// REFER TO THE ONE WHO DONE THEM PLEASE ///
    public float speed = 1f;
    float delta = 1f;
    public Transform spikes;
    //time between the movement of the spikes //
    public float delayTime;
    // second spike to move //
    public Transform spikes2;

    bool letWork = false;

    void Update()
    {
        if (letWork == true)
        {
            moveSpikes();
            movingSpikes2();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        letWork = true;
    }

    public void moveSpikes()
    {
        float y = Mathf.PingPong(speed * Time.time, delta);
        Vector3 pos = new Vector3(spikes.localPosition.x, y, spikes.localPosition.z);
        spikes.localPosition = pos;
    }


    private void movingSpikes2()
    {
        float y = Mathf.PingPong(speed * Time.time + delayTime, delta);
        Vector3 pos = new Vector3(spikes2.localPosition.x, y, spikes2.localPosition.z);
        spikes2.localPosition = pos;
    }
}
