﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingBlocks : MonoBehaviour
{
    public float speed = 1f;
    public float movement = 5f; 
    public Transform block;

    void Update() 
    {
     float y = Mathf.PingPong(speed * Time.time, movement);
     Vector3 pos = new Vector3(block.localPosition.x, y, block.localPosition.z);
     block.localPosition = pos;
    }
}
