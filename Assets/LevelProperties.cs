﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelProperties : MonoBehaviour
{
    [Header("Level Properties")]
    public bool hasKey = false;

    [Header("References")]
    public GameObject key;
    public DoorCheck doorCheck;

    [Header("Delay Properties")]
    public float delayTime = 2f;

    [Header("Scene Properties")]
    public string nextLevelName;
    public Animator anim;
    public CanvasGroup fade;

    void Update()
    {
       if(hasKey)
       {
           doorCheck.doorLocked.SetActive(false);
           doorCheck.doorUnlocked.SetActive(true);
       } 

        if(delayTime <= 0)
        {
            Debug.Log("Delay done");
            NextLevel();
        }
    }


    public void NextLevel()
    {
        anim.SetBool("LevelDone", true);
        if(fade.alpha >= 1)
        {
            SceneManager.LoadScene(nextLevelName);
        }
    }
}
